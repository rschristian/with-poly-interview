# Poly Front-End Take-Home

## Environment

- WASM Lib
  - Rust, built with `wasm-pack`

- Server
  - Vanilla Node

- Web
  - Vanilla JS

## Design

This is a rather bare-bones implementation; it makes no real attempt to provide any abstractions, as they would only add unnecessary verbosity and break up what little code there actually is.

The server is broken down into two API routes: a GET, which acts as the catch-all static asset server for the HTML, WASM build, etc., and a POST to `/generate`, which is the server computation of the Julia set. The client too is quite straight-forward, with plain HTML and a little JS for the input handler and triggering the computation, as well as communicating with the worker.

In the real world, we could start to talk about components, abstracting away the computation into a proper isomorphic interface (likely a singleton, keeping the worker running to avoid start/stop costs), but I chose not to implement any of that here for practicality's sake.

## Getting Started

Quick: Clone this repo, and then run the following:

```sh
$ node server.mjs
```

Full: If you wanted to rebuild the WASM artifacts from source (though I included them with the repo):

```sh
$ cd rs-lib

$ cargo install wasm-pack

$ wasm-pack build --target web --out-dir pkg-web

# `wasm-pack` cannot yet generate proper Node ESM, so I'd skip this unless you want to manually fix it, as I have
# $ wasm-pack build --out-dir pkg-node-esm

# Slightly easier serving of static assets for the web
$ cp pkg-web/rs_lib{.js,_bg.wasm} ../public

$ cd ..

# Now simply start the Node server
$ node server.mjs
```

At this point, the server will be listening at `http://localhost:5000` which you can open in your browser.

## Usage

The web page should explain things pretty plainly, but you'll want to provide a complex number (in the form of `-0.4,0.60`) into the input field and press `<Enter>`/click the "Submit" button. If the real and imaginary parts of the number (left and right portions, respectively) have a greater difference than 1.0, it'll pass it off to the server.

## Questions

1. > How does your client "ask" the server to run the computation? Is there a way to do this without duplicating your library SDK as an API? For example, can it be invisible to the front-end developer as to "where" the computation is run?

The client "asks" the server to run the computation via a `fetch()` request. This can absolutely be abstracted away and invisible to the front-end developer with a simple wrapper. The wrapper can make the determination of where this should be executed and all the developer needs to do is await a function call.

2. > How do you make the library non-blocking on the client?

Web Workers are an easy solution to avoid blocking the main thread, and this demo is built with one.

3. > What are the benefits and drawbacks of this way of separating compute between the client and server? Where is this a very good or very bad idea?

    - Benefits include flexibility and cost reductions for the host (who'd be paying for server upkeep), as well as almost certainly a better user experience; fast computations can be run directly on the client device, and heavier computations could be offloaded to a server. In either case, the end user benefits from the higher performance.

    - Drawbacks include multiple builds of the WASM lib (though [WinterCG](https://wintercg.org/) is standardizing here and might make this issue obsolete in the future), a bit more wiring, which means a higher maintenance burden, possible discrepancies between the two consuming runtimes, as well as possibly a very different developer experience depending on the languages used. As JS/TS only have `number` (float) and `BigInt` to express numerical types, there's no way for a WASM library to signal that it requires a signed integer, for example. Splitting compute could require more care and tests on the client than the server.

    - It can be a great system for freemium models, where paying users could get access to server resources for a task while non-paying users have to provide their own compute or local-first apps that support offline usage. In the case of an internet outage (for example), the user can still utilize the app in some capacity, even if some features are limited.

    - This might not make sense for heavier workloads, and certainly not if the WASM binary needs to be kept private, as is sometimes the case.

4. > How do you ensure consistency of your data in a scenario where the client and server are disconnected?

It depends a bit on the specific architecture or use case, but if it's a correctness issue, background or deferred confirmations with the server are an option, or simply confirming with the server at the end of a workflow, as you might see when selecting a new password for a website. While you'll often have a client-side implementation of the password requirements to provide instant feedback, it'll eventually need to go through the server for confirmation.

## Extra Credit Questions

1. > Can you have the client automatically decide when to use the server vs the client?

Absolutely, as done in this demo, simply use some input condition to decide what needs to take place where.

2. > What kinds of computations would need to be on one or the other?

Media conversions where client-side codecs are often limited are a great example, as would be any computations where we can't expose the logic to the end user in any fashion.

### Notes

I mention this in the rs-lib source, but the Julia set generator is simply a copy/pasted example, not my own code. I figured it made for a better demo than an `add()` function is all.
