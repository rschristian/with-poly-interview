import * as path from 'node:path';
import * as fs from 'node:fs';
import { createServer } from 'http';
import { generate as wasmGenerate } from './rs-lib/pkg-node-esm/rs_lib.js';

const PUBLIC_DIR = path.join(process.cwd(), './public');

const CONTENT_TYPE = {
    html: 'text/html; charset=utf-8',
    js: 'application/javascript',
    wasm: 'application/wasm',
    json: 'application/json',
};

async function tryServePublic(req) {
    let asset = req.url;
    if (asset == '/') asset = '/index.html';

    const contentType = path.extname(asset).substr(1);

    const filePath = path.join(PUBLIC_DIR, asset);
    const stream = fs.createReadStream(filePath);

    return { stream, contentType };
}

function getPostData(req) {
    return new Promise((res, rej) => {
        try {
            let body = '';
            req.on('data', (d) => {
                body += d;
            });

            req.on('end', () => {
                res(body);
            });

            req.on('error', (e) => {
                rej(e);
            });
        } catch (e) {
            rej(e);
        }
    });
}

const simpleLog = (req) => console.debug(`${req.method}: ${req.url}`);
const error = (res, code, msg) =>
    res.writeHead(code, { 'Content-Type': 'application/json' }).end(JSON.stringify({ error: msg }));

createServer(async (req, res) => {
    simpleLog(req, res);

    if (req.method == 'GET') {
        const asset = await tryServePublic(req);
        res.setHeader('Content-Type', CONTENT_TYPE[asset.contentType] || 'text/plain');

        asset.stream.on('error', () => {
            error(res, 404, '404: Not Found');
        });

        asset.stream.on('ready', () => {
            asset.stream.pipe(res);
        });
    } else if (req.method == 'POST' && req.url == '/generate') {
        res.setHeader('Content-Type', CONTENT_TYPE.json);

        let body;
        try {
            body = await getPostData(req);
        } catch (e) {
            return error(res, 400, e);
        }
        const { re, im } = JSON.parse(body);
        if (!re || !im) return error(res, 400, 'Missing re or im');

        return res.end(wasmGenerate(re, im), 'binary');
    }
}).listen(5000, () => {
    console.log('> Running on localhost:5000');
});
