import init, { generate } from './rs_lib.js';

addEventListener('message', e => {
    init().then(() => {
        const { re, im } = e.data;
        postMessage(generate(re, im));
    });
});
