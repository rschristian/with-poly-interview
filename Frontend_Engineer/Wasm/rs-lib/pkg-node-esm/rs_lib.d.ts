/* tslint:disable */
/* eslint-disable */
/**
* @param {number} re
* @param {number} im
* @returns {Uint8Array}
*/
export function generate(re: number, im: number): Uint8Array;
